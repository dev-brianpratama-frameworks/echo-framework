# About this framework
This framework is based on [Echo](https://echo.labstack.com) by [Labstack](https://labstack.com)

## How to run this app

###  Prepare your PostgreSQL database

1.  Switch user to postgres and run psql.
    ```
    sudo -i -u postgres
    psql
    ```

2.  Create a new database.
    ```
    CREATE DATABASE <YOUR_DATABASE_NAME>;
    ```

3.  Create a new user with password and grant all privileges on the database to this user, then quit from psql.
    ```
    CREATE ROLE <YOUR_DB_USER> WITH LOGIN PASSWORD '<YOUR_DB_PASSWORD>' CREATEDB;
    GRANT ALL PRIVILEGES ON DATABASE <YOUR_DATABASE_NAME> TO <YOUR_DB_USER>;
    ```

4.  Exit from psql and postgres.
    ```
    \q
    exit
    ```

4.  Replace db_user in migrate.sql with your created database user name and import migrate.sql to the database you just created.
    ```
    sudo -u postgres psql <YOUR_DATABASE_NAME> < migrate.sql
    ```

###  Prepare Echo environment and run

1.  Clone this repo with your own desired project name.
    ```
    git clone https://gitlab.com/dev-brianpratama-frameworks/echo-framework.git <YOUR_PROJECT_NAME>

1.  Prepare your own environtments in Echo by editing config.json and config-pg.json with your own data.

2.  Install dependencies to make sure that everything runs perfectly.
    ```
    go get .
    ```

4.  To run Echo with config environment, run
    ```
    go run .
    ```
    To run Echo with config-pg environment, run
    ```
    go run . pg
    ```

5.  Go to http://localhost:5000

### Generate Swagger documentation and run it

1.  Download Swag for Go.
    ```
    go get github.com/swaggo/swag/cmd/swag
    ```

2.  Make sure that your GO Path is on the PATH environment variable.
    ```
    export PATH=$(go env GOPATH)/bin:$PATH
    ```

3.  Generate Swagger documentation.
    ```
    swag init --parseDependency --parseInternal .
    ```

4.  The generated Swagger documentation is saved in docs folder.

5.  Go to http://localhost:5000/docs/index.html to run Swagger documentation.

###  How to dockerize

1.  Create docker image and delete the old unused image
    ```
    docker build —rm -t image_name .
    ```

2.  Run docker container using the image
    ```
    docker run -it -p 8081:5000 image_name:latest
    ```

3.  Go to http://localhost:8081

###  What methods are ready to use

####  _Register_

To register, POST /register with the following credential
```
{
    "email": "example@mail.com",
    "password": "passwordexample",
    "username": "myusername",
    "created_by": "guest",
    "updated_by": "guest"
}
```

####  _Login_

To login, POST /login with the following credential
```
{
    "email": "example@mail.com",
    "password": "passwordexample"
}
```

##  Features

### _Echo_
A Golang library by Labstack. Full documentation can be seen [here](https://echo.labstack.com/guide).

### _Swagger for Echo_
For the documentation, this framework uses [echo-swagger](https://github.com/swaggo/echo-swagger). See the declarative comment format [here](https://github.com/swaggo/swag#declarative-comments-format).

### _Viper_
Viper is used to read JSON files. So Viper can read environments from JSON files, which are more well structured than .env files. Full documentation can be seen [here](https://github.com/spf13/viper).

##  Pattern

### Models
Define models of requests and responses.

### Repositories
Execute database queries.

### Controllers
Handle requests to create responses by using models and repositories.
