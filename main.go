package main

import (
	"net/http"
	"time"

	_ "echo-framework/docs"

	"github.com/swaggo/echo-swagger"

	"github.com/spf13/viper"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	cfg "echo-framework/src/config"

	r "echo-framework/src/router"
)

// @title EchoAPI
// @version 1.0
// @description API documentation for Echo API

type H map[string]interface{}

func main() {
	cfg.InitConfig()
	// Create echo instance
	e := echo.New()
	// Hide echo banner
	e.HideBanner = true
	// Debug model
	e.Debug = viper.GetBool("debug")

	// Create route for documentation
	e.GET("/docs/*", echoSwagger.WrapHandler)

	// CORS Config
	CORSConfig := middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}

	// API
	auth := e.Group("/auth")
	api := e.Group("/api")
	landing := e.Group("")

	auth.Use(middleware.CORSWithConfig(CORSConfig))
	api.Use(middleware.CORSWithConfig(CORSConfig))

	api.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey:    []byte(viper.GetString("jwt_secret")),
		SigningMethod: "HS512",
	}))

	//register route from router packages
	r.AuthRouter(auth)
	r.AuthUserRouter(api)

	landing.Any("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, H{
			"status":   "200",
			"msg":      "success",
		})
	})

	// start to listen
	s := &http.Server{
		Addr:         ":" + viper.GetString("app_port"),
		ReadTimeout:  time.Duration(viper.GetInt("app_read_timeout")) * time.Second,
		WriteTimeout: time.Duration(viper.GetInt("app_write_timeout")) * time.Second,
	}
	e.Logger.Fatal(e.StartServer(s))
}