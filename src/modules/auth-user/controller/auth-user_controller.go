package controller

import (
	"net/http"
	repo "echo-framework/src/modules/auth-user/repository"
	// mdl "echo-framework/src/modules/auth/model"
	s "echo-framework/src/modules/shared"

	"github.com/labstack/echo/v4"
)

// @Description Get all users
// @Tags Auth
// @Param Authorization header string true "Insert your access token" default(Bearer <Add access token here>)
// @Accept json
// @Produce json
// @Success 200 {object} model.ListResponse
// @Router /api/users [get]
func GetAll() echo.HandlerFunc {
	return func(c echo.Context) error {

		list, _ := repo.GetAll()

		return c.JSON(http.StatusOK, s.H{
			"status": 200,
			"msg":    "success",
			"data":   list,
		})
	}
}
