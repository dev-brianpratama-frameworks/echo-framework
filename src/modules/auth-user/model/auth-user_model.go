package model

import (
	"time"
)

// TUser represents a row from 'public.t_user'.
type TUser struct {
	ID						int						`json:"id" db:"id"`
	Email					string				`json:"email" db:"email"`
	Username			string				`json:"username" db:"username"`
	CreatedBy			int						`json:"created_by" db:"created_by"`
	UpdatedBy			int						`json:"updated_by" db:"updated_by"`
	CreatedAt			time.Time			`json:"created_at" db:"created_at"`
	UpdatedAt			time.Time			`json:"updated_at" db:"updated_at"`
}

type ListResponse struct {
	Status				int						`json:"status"`
	Msg						string				`json:"msg"`
	List					[]TUser				`json:"list"`
}
