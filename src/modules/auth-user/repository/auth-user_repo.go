package repository

import (
	"database/sql"
	h "echo-framework/src/helpers"
	cfg "echo-framework/src/config"
	mdl "echo-framework/src/modules/auth-user/model"
	s "echo-framework/src/modules/shared"

	"github.com/labstack/gommon/log"
	"github.com/spf13/viper"
)

var (
	dbg = viper.GetBool("debug")
	db  = *cfg.DB
)

func GetAll() ([]mdl.TUser, error) {
	ctx, cancel := h.SetQueryContext()
	defer cancel()

	query := `
	SELECT
		id,
		email,
		username,
		created_by,
		updated_by,
		created_at,
		updated_at
	FROM t_user
	`
	if s.DBG {
		h.Dlog(query, "")
	}
	var list []mdl.TUser

	// Replace the '?' in our sql statement with vars
	rows, err := s.DB.QueryxContext(ctx, query)
	// Exit if we get an error

	if err == sql.ErrNoRows {
		log.Info("Request failed : no rows")
	} else if err != nil {
		log.Error(err)
	} else {

		for rows.Next() {
			d := mdl.TUser{}
			err = rows.StructScan(&d)
			// Exit if we get an error
			if err != nil {
				log.Error(err)
			}
			list = append(list, d)
		}

		if err = rows.Err(); err != nil {
			log.Error(err)
		}
		defer rows.Close()

	}
	return list, nil
}
