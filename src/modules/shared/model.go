package shared

import (
	"github.com/spf13/viper"
	cfg "echo-framework/src/config"
)

var (
	DBG = viper.GetBool("debug")
	DB  = *cfg.DB
)
type H map[string]interface{}

