package model

import (
	"time"
)

// LoginReq represents a login request from user
type LoginReq struct {
	Email					string				`json:"email"`
	Password			string				`json:"password"`
}

type RegReq struct {
	Email					string				`json:"email"`
	Password			string				`json:"password"`
	Username			string				`json:"username"`
	CreatedBy			int						`json:"created_by"`
	UpdatedBy			int						`json:"updated_by"`
}

type RegRes struct {
	ID						int
	Msg						string
	Status				int
}

// TUser represents a row from 'public.t_user'.
type TUser struct {
	ID						int						`db:"id"`
	Email					string				`db:"email"`
	Password			string				`db:"password"`
	Username			string				`db:"username"`
	CreatedBy			int						`db:"created_by"`
	UpdatedBy			int						`db:"updated_by"`
	CreatedAt			time.Time			`db:"created_at"`
	UpdatedAt			time.Time			`db:"updated_at"`
}

type LoginRes struct {
	Tokens				[]Tokens				`json:"tokens"`
}

type Tokens struct {
	AccessTokens		string				`json:"access_token"`
	RefreshToken		string				`json:"refresh_token"`
}