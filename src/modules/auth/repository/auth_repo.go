package repository

import (
	"database/sql"
	h "echo-framework/src/helpers"
	cfg "echo-framework/src/config"
	mdl "echo-framework/src/modules/auth/model"

	"github.com/labstack/gommon/log"
	"github.com/spf13/viper"
)

var (
	dbg = viper.GetBool("debug")
	db  = *cfg.DB
)

// CreateUser insert new user
func CreateUser(t *mdl.RegReq) (int, error) {
	ctx, cancel := h.SetQueryContext()
	defer cancel()

	query := `
	INSERT INTO
		t_user (
			email,
			password,
			username,
			created_by,
			updated_by
		)
	VALUES ($1, $2, $3, $4, $5)
	RETURNING id
	`
	db.Rebind(query)

	if dbg {
		log.Infof(query)
		log.Info(t)
	}
	hashedPassword, _ := h.HashPassword(t.Password)
	id := 0
	// Replace the '?' in our sql statement with vars
	err := db.QueryRowContext(
		ctx,
		query,
		t.Email,
		hashedPassword,
		t.Username,
		t.CreatedBy,
		t.UpdatedBy,
	).Scan(&id)
	// Exit if we get an error
	if err != nil {
		log.Error(err)
		return -1, err
	}
	return id, err
}

func FindUser(email string) (mdl.TUser, error) {
	query := `
		SELECT
			*
		FROM
			"t_user"
		WHERE
			email = $1
		`

	if dbg {
		h.Dlog(query, email)
	}

	var err error
	rows := mdl.TUser{}
	err = db.QueryRowx(query, email).StructScan(&rows)
	// Exit if the SQL doesn't work for some reason
	if err == sql.ErrNoRows {
		log.Info(err)
	} else if err != nil {
		log.Error(err)
		return rows, err
	}

	return rows, nil
}
