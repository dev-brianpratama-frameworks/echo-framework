package controller

import (
	"net/http"
	repo "echo-framework/src/modules/auth/repository"
	mdl "echo-framework/src/modules/auth/model"
	h "echo-framework/src/helpers"
	s "echo-framework/src/modules/shared"

	"github.com/davecgh/go-spew/spew"
	"github.com/labstack/echo/v4"
)

// @Description Create a new user
// @Tags Auth
// @Accept json
// @Produce json
// @Param credential body mdl.RegReq true "credential"
// @Success 200 {object} mdl.RegRes
// @Router /auth/register [post]
func CreateUser() echo.HandlerFunc {
	return func(c echo.Context) error {
		req := new(mdl.RegReq)
		if err := c.Bind(req); err != nil {
			return c.String(http.StatusBadRequest, "Bad Request")
		}
		if req.Email == ""||
			req.Password == "" ||
			req.Username == "" {
			return c.String(http.StatusBadRequest, "Bad Request")
		}
		id, err := repo.CreateUser(req)
		if err != nil {
			return err
		}

		return c.JSON(http.StatusCreated, s.H{
			"status": 200,
			"msg":    "success",
			"id":   id,
		})
	}
}

// @Description Login to get token
// @Tags Auth
// @Accept  json
// @Produce  json
// @Param credential body mdl.LoginReq true "credential"
// @Success 200 {object} mdl.LoginRes
// @Router /auth/login [post]
func LoginUser() echo.HandlerFunc {
	return func(c echo.Context) error {

		req := new(mdl.LoginReq)
		if err := c.Bind(req); err != nil {
			return c.String(http.StatusBadRequest, "Bad Request")
		}
		
		getUser, _ := repo.FindUser(req.Email)
		if getUser.Email == "" {
			return c.String(http.StatusBadRequest, "Bad Request")
		}

		pw := req.Password

		if h.CheckPasswordHash(pw, getUser.Password) {
			tokens, claims, _ := h.GenerateJWT(getUser)
			spew.Dump(claims)

			return c.JSON(http.StatusOK, s.H{
				"tokens":    tokens,
			})

		}

		return c.String(http.StatusUnauthorized, "unauthorized")

	}
}
