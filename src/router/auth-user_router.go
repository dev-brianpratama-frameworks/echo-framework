package router

import (
	ct "echo-framework/src/modules/auth-user/controller"

	"github.com/labstack/echo/v4"
)

// AuthRouter ...
func AuthUserRouter(auth *echo.Group) {
	auth.OPTIONS("/users", ct.GetAll())
	auth.GET("/users", ct.GetAll())
}
