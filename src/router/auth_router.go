package router

import (
	ct "echo-framework/src/modules/auth/controller"

	"github.com/labstack/echo/v4"
)

// AuthRouter ...
func AuthRouter(auth *echo.Group) {
	auth.OPTIONS("/login", ct.LoginUser())
	auth.POST("/login", ct.LoginUser())

	auth.OPTIONS("/register", ct.CreateUser())
	auth.POST("/register", ct.CreateUser())
}
