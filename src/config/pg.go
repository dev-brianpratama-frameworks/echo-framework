package config

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"log"
	"os"
	"time"
)

var DB *sqlx.DB

//InitDBPg
func init() {
	viper.SetConfigName("config") // name of config file (without extension)
	argsLen := len(os.Args)
	log.Println(os.Args)
	if argsLen > 1 {
		if os.Args[1] == "pg" || os.Args[2] == "pg" {
			viper.SetConfigName("config-pg" ) // name of config file (without extension)
		}
	}

	viper.AddConfigPath(".")      // optionally look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Panic(fmt.Errorf("Fatal error reading config file: %s \n", err))
	}

	// Create connection string

	connString := fmt.Sprintf("host=%s user=%s password=%s port=%d dbname=%s sslmode=%s",
		viper.Get("db.pg.host"),
		viper.Get("db.pg.username"),
		viper.Get("db.pg.password"),
		viper.GetInt("db.pg.port"),
		viper.Get("db.pg.database"),
		viper.Get("db.pg.ssl_mode"),
	)

	//defer db.Close()

	// Create connection pool
	DB, err = sqlx.Open("postgres", connString)

	if err != nil {
		log.Println("Error creating connection pool: " + err.Error())
	}
	DB.SetMaxOpenConns(viper.GetInt("db.pg.maxOpenConnection")) // Sane default
	DB.SetMaxIdleConns(viper.GetInt("db.pg.maxIdleConnecton"))

	DB.SetConnMaxLifetime(time.Minute * 10)

	//log.Printf("Connected!\n")

	if err = DB.Ping(); err != nil {
		log.Panic(err)
	}
}
