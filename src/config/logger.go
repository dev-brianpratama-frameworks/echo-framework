package config

import (
	"io"
	"os"
	"time"

	"github.com/labstack/gommon/log"
)

// LogMultiWriter ...
var LogMultiWriter io.Writer = io.MultiWriter(os.Stdout, NewLogFile())

// LogSQL ...
var LogSQL io.Writer = io.MultiWriter(os.Stdout, NewSQLLogFile())

func init() {
	log.SetOutput(LogMultiWriter)
	//log.EnableColor()
	log.SetHeader(`{"${level}":"=>",time":"${time_rfc3339_nano}","prefix":"${prefix}","file":"${short_file}","line":"${line}"}`)
}

// TodayFilename ...
func TodayFilename() string {
	today := time.Now().Format("2006-01-02")
	return today + ".log"
}

// NewLogFile ...
func NewLogFile() *os.File {
	filename := "logs/" + TodayFilename()
	// open an output file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return f
}

// NewSQLLogFile ...
func NewSQLLogFile() *os.File {
	filename := "logs/SQL-" + TodayFilename()
	// open an output file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}

	//defer f.Close()
	return f
}
