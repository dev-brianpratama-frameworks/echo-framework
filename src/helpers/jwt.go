package helpers

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/labstack/echo/v4"
	"echo-framework/src/config"
	"time"

	mdl "echo-framework/src/modules/auth/model"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
)

// GenerateJWT ...
func GenerateJWT(u mdl.TUser) (map[string]string, jwt.MapClaims, error) {
	config.InitConfig()

	// Set refresh Token
	refreshToken := jwt.New(jwt.SigningMethodHS512)
	rtClaims := refreshToken.Claims.(jwt.MapClaims)
	rtClaims["sub"] = 1
	rtClaims["exp"] = time.Now().Add(time.Hour * time.Duration(viper.GetInt("jwt_refresh_token_lifetime"))).Unix()
	rt, err := refreshToken.SignedString([]byte(viper.GetString("jwt_secret")))
	if err != nil {
		return nil, nil, err
	}

	// Set claims
	token := jwt.New(jwt.SigningMethodHS512)
	claims := token.Claims.(jwt.MapClaims)
	claims["uid"] = u.ID
	claims["email"] = u.Email
	claims["username"] = u.Username
	claims["admin"] = true
	claims["roles"] = []string{"ADMIN", "USER"}
	claims["exp"] = time.Now().Add(time.Minute * time.Duration(viper.GetInt("jwt_access_token_lifetime"))).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(viper.GetString("jwt_secret")))
	if err != nil {
		return nil, nil, err
	}
	tokens := map[string]string{
		"access_token":  t,
		"refresh_token": rt,
	}
	return tokens, claims, nil
}

func JWTGetClaimsUID(c echo.Context) (string, error){

	bearer := c.Request().Header.Get("Authorization")
	tokenString := bearer[7:]
	spew.Dump("----- token string -----", tokenString)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		hmacSampleSecret := []byte(viper.GetString("jwt_secret"))
		return hmacSampleSecret, nil
	})

	if err != nil {
		return "error", err
	}
	//spew.Dump("----- token parse -----", token.Claims.(jwt.MapClaims)["uid"])
	return token.Claims.(jwt.MapClaims)["uid"].(string), nil
}
