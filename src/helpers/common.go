package helpers

import (
	"context"
	"encoding/base64"
	"fmt"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"time"

	"echo-framework/src/config"

	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/viper"
)

// Float64ToString ...
func Float64ToString(number float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(number, 'f', 0, 64)
}

// Int64ToString ...
func Int64ToString(number int64) string {
	// to convert a int64 number to a string
	return strconv.FormatInt(number, 10)
}

// Base64Encode ...
func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

// Base64Decode ...
func Base64Decode(str string) string {
	d, _ := base64.StdEncoding.DecodeString(str)
	return string(d)
}

// Base64URLEncode ...
func Base64URLEncode(str string) string {
	return base64.URLEncoding.EncodeToString([]byte(str))
}

// Base64URLDecode ...
func Base64URLDecode(str string) string {
	d, _ := base64.URLEncoding.DecodeString(str)
	return string(d)
}

// CleanString ...
func CleanString(str string) string {
	s := strings.TrimSpace(str)
	return s
}

// SourceTrace ...
func SourceTrace(depthList ...int) string {
	var depth int
	if depthList == nil {
		depth = 2
	} else {
		depth = depthList[0]
	}
	function, file, line, _ := runtime.Caller(depth)
	return fmt.Sprintf("File: %s  Function: %s Line: %d", chopPath(file), runtime.FuncForPC(function).Name(), line)
}

// return the source filename after the last slash
func chopPath(original string) string {
	i := strings.LastIndex(original, "/")
	if i == -1 {
		return original
	}
	return original[i+1:]

}

// Dlog ...
func Dlog(msg string, i interface{}) {
	spew.Fprintln(config.LogSQL, "\n\ntime : ", time.Now().Format(time.RFC3339Nano), "\nQUERY : \n", msg,
		"\nparams : \n", spew.Sdump(i), "\n", SourceTrace())
}

// InArray ...
func InArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}
	//return is in array and position of data
	return
}

// SetQueryContext ...
func SetQueryContext() (context.Context, context.CancelFunc) {
	d := time.Now().Add(time.Millisecond * time.Duration(viper.GetInt("db.pg.query_exec_timeout")))
	ctx := context.Background()
	ctx, cancel := context.WithDeadline(ctx, d)

	return ctx, cancel
}

func times(str string, n int) (out string) {
	for i := 0; i < n; i++ {
		out += str
	}
	return
}

// PadLeft left-pads the string with pad up to len runes
// len may be exceeded if
func PadLeft(str string, pad string, length int) string {
	return times(pad, length-len(str)) + str
}

// PadRight right-pads the string with pad up to len runes
func PadRight(str string, pad string, length int) string {
	return str + times(pad, length-len(str))
}

// MakeTimestamp generate unix time in millisecond
func MakeTimestamp() string {
	return time.Now().Format("20060102150405.000000")
}
