FROM golang:1.16
ENV PACKAGE_PATH=app
RUN mkdir -p /go/src/
WORKDIR /go/src/$PACKAGE_PATH
COPY . /go/src/$PACKAGE_PATH/
RUN go get
RUN go build -o app
ENTRYPOINT ./app
EXPOSE 5000
